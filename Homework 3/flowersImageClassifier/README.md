### Project Overview
This experiments are for the purposes of *SUTD 50.039, Theory and Practice of Deep learning* only

### Project Files
`part1.ipynb` and `part2.ipynb` files contains the Jupyter Notebook for part 1 and part 2 respectively. The cells can be run in order or in random sequence, doesn't matter, still works.

The files `model_ic.py`, `utils_ic.py` contains functions for the training and testing of models.

`models` contains the models that I have trained according to `part2.ipynb`.
`results` contains the results that for the respective questions.

The reports and analysis are inline in the ipynb files.

### Setup
To install all the required modules, run `pip install -r requirements.txt`.
Copy the `flower` dir from https://github.com/MiguelAMartinez/flowers-image-classifier here.

### Data
This project uses the [102 Category Flower Dataset](http://www.robots.ox.ac.uk/~vgg/data/flowers/102/index.html) from the University of Oxford. It consist of 102 categories of flowers, each containing 40 to 258 images.

## Deep Learning with PyTorch - Image Classifier
This project is adapted from https://github.com/MiguelAMartinez/flowers-image-classifier.